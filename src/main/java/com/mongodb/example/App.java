package com.mongodb.example;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws UnknownHostException {
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        DB db = mongoClient.getDB("hockey");
        DBCollection collection = db.getCollection("players");
        System.out.println("Collection: " + collection);

        System.out.println("Document count: " + collection.getCount());

        for (String col : db.getCollectionNames()
                ) {
            System.out.println("Collection: "+ col);
        }

    }
}
